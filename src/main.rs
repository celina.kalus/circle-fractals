extern crate cairo;

use cairo::{ ImageSurface, Format, Context };
use std::fs::File;
use std::f64::consts::PI;
use std::ops::{Add, Sub, Mul};


#[derive(Debug, Copy, Clone, PartialEq)]
struct Vector2D {
    x: f64,
    y: f64,
}


impl Add for Vector2D {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self { x: self.x + other.x, y: self.y + other.y }
    }
}

impl Sub for Vector2D {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self { x: self.x - other.x, y: self.y - other.y }
    }
}

impl Mul<f64> for Vector2D {
    type Output = Vector2D;

    fn mul(self, rhs: f64) -> Vector2D {
        Vector2D { x: self.x * rhs, y: self.y * rhs }
    }
}

impl Mul<Vector2D> for f64 {
    type Output = Vector2D;

    fn mul(self, rhs: Vector2D) -> Vector2D {
        Vector2D { x: self * rhs.x, y: self * rhs.y }
    }
}

trait Length {
    fn length(self) -> f64;
}

impl Length for Vector2D {
    fn length(self) -> f64 {
        (self.x * self.x + self.y * self.y).sqrt()
    }
}


#[derive(Debug, Copy, Clone, PartialEq)]
struct Color {
    r: f64,
    g: f64,
    b: f64,
}

fn draw_circle(context: &Context, pos: &Vector2D, radius: f64, color: &Color) {
    context.set_source_rgb(color.r, color.g, color.b);
    context.arc(pos.x, pos.y, radius, 0.0, 2.0 * PI);
    context.fill();
}

fn draw_line(context: &Context, pos1: &Vector2D, pos2: &Vector2D, color: &Color) {
    context.set_source_rgb(color.r, color.g, color.b);
    context.move_to(pos1.x, pos1.y);
    context.line_to(pos2.x, pos2.y);
    context.stroke();
}

fn main() {
    let surface = ImageSurface::create(Format::ARgb32, 600, 600)
        .expect("Couldn't create surface");

    let context = Context::new(&surface);

    context.set_source_rgb(1.0, 1.0, 1.0);
    context.paint();


    // three somewhat arbitrary points
    let p1 = Vector2D { x: 000.0, y: 000.0 };
    let p2 = Vector2D { x: 400.0, y: 000.0 };
    let p3 = Vector2D { x: 000.0, y: 100.0 };

    // vectors between each of the points
    let l12 = p2 - p1;
    let l23 = p3 - p2;
    let l13 = p3 - p1;

    // lengths of sides of the triangle
    let a = l12.length();
    let b = l23.length();
    let c = l13.length();

    println!("{} {} {}", a, b, c);

    // calculate the radii of three circles 
    // with midpoints p1, p2, p3 such that
    // all three circles touch
    // (this is done by solving a linear system)
    let r3 = 0.5*(c-a+b);
    let r2 = b-r3;
	let r1 = a-r2;


    let k1 = 1.0 / r1;
    let k2 = 1.0 / r2;
    let k3 = 1.0 / r3;


    let k4 = k1 + k2 + k3 + (k1 * k2 + k2 * k3 + k1 * k3).sqrt();
    let r4 = 1.0 / k4;

    println!("Radius: {}", r4);


    let cx2 = 2.0 * (p2.x - p1.x);
    let cy2 = 2.0 * (p2.y - p1.y);
    let cx3 = 2.0 * (p3.x - p1.x);
    let cy3 = 2.0 * (p3.y - p1.y);


    let c2 = (r1 + r4) * (r1 + r4) - (r2 + r4) * (r2 + r4) - (p1.x * p1.x - p2.x * p2.x) - (p1.y * p1.y - p2.y * p2.y);
    let c3 = (r1 + r4) * (r1 + r4) - (r3 + r4) * (r3 + r4) - (p1.x * p1.x - p3.x * p3.x) - (p1.y * p1.y - p3.y * p3.y);


    let det = cx2 * cy3 - cy2 * cx3;

    let m = Vector2D {
            x: (cy3 * c2 - cx3 * c3) / det,
            y: (cx2 * c3 - cy2 * c2) / det
        };

    println!("Position: {}, {}", m.x, m.y);


    let gray = Color {
            r: 0.5,
            g: 0.5,
            b: 0.5,
        };

    let red = Color {
            r: 1.0,
            g: 0.0,
            b: 0.0,
        };

    let blue = Color {
            r: 0.0,
            g: 0.0,
            b: 1.0
        };



    draw_circle(&context, &p1, r1, &gray);
    draw_circle(&context, &p2, r2, &gray);
    draw_circle(&context, &p3, r3, &gray);

    draw_circle(&context, &m, r4, &red);


    let mut file = File::create("output.png")
        .expect("Couldn't create file");

    surface.write_to_png(&mut file)
        .expect("Couldn't write to png");

    println!("Hello, world!");
}
